<?php
	class RRouter {
		public $routes = [];
		public $method;
		public $url;
		public $x404;

		/**
		 * Construct the router, get the method and URL
		 */
		public function __construct() {
			// Get HTTP Method
			$this->method = $_SERVER['REQUEST_METHOD'];

			// Get URL
			$this->url = $_SERVER["REQUEST_URI"];
			if (strstr($_SERVER["REQUEST_URI"], "?")) {
				$this->url = explode("?", $_SERVER["REQUEST_URI"])[0];
			}
		}

		/**
		 * Start the router, handle routes and call appropriate function,
		 * if no routes are handled the 404 function will be called
		 */
		public function start() {
			$handled = false;

			if (isset($this->routes[$this->method])) {
				foreach ($this->routes[$this->method] as $route) {
					if (preg_match_all('#^' . $route["pattern"] . '$#', $this->url, $matches, PREG_SET_ORDER)) {
						$args = array_slice($matches[0], 1);

						switch ($this->method) {
							case 'GET':
								$args = array_merge($args, $_GET);
								break;

							case "PUT":
								$args = array_merge($args, json_decode(@file_get_contents('php://input'), true));
								break;

							case "POST":
								$args = array_merge($args, json_decode(@file_get_contents('php://input'), true));
								break;
						}

						$handled = true;
						call_user_func_array($route["function"], $args);
					}
				}
			}

			if ($handled == false) {
				if (isset($this->x404)) {
					call_user_func_array($this->x404, [$this->method, $this->url]);
				}
			}
		}

		/**
		 * Set a function to be called when no route match.
		 */
		public function x404($function) {
			$this->x404 = $function;
		}

		/**
		 * Create a GET route
		 * @param  [type] $url      url pattern
		 * @param  [type] $function function callback
		 */
		public function get($url, $function) {
			$this->routes["GET"][] = [
				"pattern" => $url,
				"function" => $function
			];
		}

		/**
		 * Create a POST route
		 * @param  [type] $url      url pattern
		 * @param  [type] $function function callback
		 */
		public function post($url, $function) {
			$this->routes["POST"][] = [
				"pattern" => $url,
				"function" => $function
			];
		}

		/**
		 * Create a PUT route
		 * @param  [type] $url      url pattern
		 * @param  [type] $function function callback
		 */
		public function put($url, $function) {
			$this->routes["PUT"][] = [
				"pattern" => $url,
				"function" => $function
			];
		}

		/**
		 * Create a DELETE route
		 * @param  [type] $url      url pattern
		 * @param  [type] $function function callback
		 */
		public function delete($url, $function) {
			$this->routes["DELETE"][] = [
				"pattern" => $url,
				"function" => $function
			];
		}
	}
?>