# RRouter (REST Router)

RRouter is used to route URLs in a RESTful API backend (PHP). It's simple to set-up and simple to handle dynamic routes.

## Usage

Create an instance of the RRouter class:

	<?php
		require_once("RRouter.php");
		$router = new RRouter();
	?>

Then setup the routes/urls and complementing actions:

	<?php
		$router->post("/login", function($email, $password) {
			// login
			// $email and $password are form data
		});
	
		$router->get("/current-user", function() {
			// return current user data
		});
	
		$router->delete("/item/(\w+)", function($item) {
			// delete $item
			// $item is retrieved from (\w+) in URL
		});
	
		$router->get("/item/(\w+)", function($id) {
			// get item with id of $id
			// $id is retrieved from (\w+) in URL
		});
	?>

## 404

You can setup a generic 404 response using the function x404():

	<?php
		$router->x404(function($method, $url) {
			// view 404 page
		});
	?>

## Dynamic Routing

If you want to handle dynamic routes you can use the x404() function to parse URL/method and invoke actions accordingly.
Here is a small example which includes a module from "modules" folder using the first part of the URL then invoking a method according to the HTTP method.

	<?php
		$router->x404(function($method, $url) {
			$bits = explode("/", substr($url, 1));
			$module = count($bits) > 0 ? $bits[0] : "";
			$id = count($bits) > 1 ? $bits[1] : "";
	
			if (file_exists("modules/{$module}.php")) {
				switch ($method) {
					case "GET":
						$function = "get";
						break;
	
					case "PUT":
						$function = "addNew";
						$args = json_decode(@file_get_contents('php://input'));
						break;
	
					case "POST":
						$function = "save";
						$args = json_decode(@file_get_contents('php://input'));
						break;
	
					case "DELETE":
						if ($id) {
							$function = "delete";
							$args = array_merge(["id" => $id], $_GET);
						}
						break;
				}
	
				// Call the correct function and print the result
				require_once "modules/{$module}.php";
				print call_user_func([$module, $function], $args);
			} else {
				// 404 response
			}
		});
	?>

## Start Router

To start the router call method start()

	<?php
		$router->start();
	?>
